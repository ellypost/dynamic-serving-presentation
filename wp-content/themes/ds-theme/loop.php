<?php
while(have_posts()) {
	the_post();
	the_title("<h1>", "</h1>");
	?>
	<article><?=get_the_content();?></article>
	<?php
}
?>