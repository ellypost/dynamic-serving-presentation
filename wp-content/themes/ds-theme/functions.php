<?php

$themePath = trailingslashit(get_stylesheet_directory());
require_once $themePath . 'src/php-vendor/autoload.php';

// set up our mobile detect global var
$GLOBALS['detect'] = new \Jenssegers\Agent\Agent();



/**
 * returns a bool value if it's a mobile user or not
 * Tablets not included as mobile
 * @uses MobileDetect
 * @return bool
 */
function is_mobile()
{
	global $detect;

	if(is_desktop_mode())
		return false;

	if($detect->isMobile() || $detect->isiPhone())
		return true;
	return false;
}

/**
 * returns a bool value if it's a tablet user or not
 * @uses MobileDetect
 * @return bool
 */
function is_tablet()
{
	global $detect;

	if(is_desktop_mode())
		return false;

	if($detect->isTablet())
		return true;
	return false;
}

/**
 * returns a bool value if it's a desktop user or not
 * @uses MobileDetect
 * @return bool
 */
function is_desktop()
{
	if(!is_tablet() && !is_mobile())
		return true;
	return false;
}

/**
 * Checks for a user override of desktop mode
 */
function is_desktop_mode()
{
	return isset($_COOKIE['desktopMode']);
}


