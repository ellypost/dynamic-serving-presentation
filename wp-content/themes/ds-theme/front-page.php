<?php

header('Vary: User-Agent');

get_header();

global $detect;

if (is_tablet())
	get_template_part('front-page', 'jeff');
else if (is_mobile())
	get_template_part('front-page', 'mobile');
else
	get_template_part('front-page', 'desktop');

get_footer();