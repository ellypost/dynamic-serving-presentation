<?php
global $detect;

// add browser as a body class
// enum: edge, ie, safari, chrome, firefox, ...
$browser = strtolower($detect->browser());
$extraBodyClass = $browser;
if (is_mobile() )
	$extraBodyClass .= " mobile";
elseif (is_tablet() )
	$extraBodyClass .= " tablet";
else
	$extraBodyClass .= " desktop";

if (is_desktop_mode() )
	$extraBodyClass .= " desktop-mode";
?><!DOCTYPE html>
<html id="wp-override" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Language" content="en_US">

    <title><?php if (is_front_page() ) { echo "&#187; "; echo get_bloginfo('description'); } else { wp_title(); } ?></title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="profile" href="http://gmpg.org/xfn/11" >
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php


	wp_head();

	if (is_mobile()) {
		?>
        <link rel="stylesheet"  href="<?=get_template_directory_uri();?>/style-mobile.css">
		<?php
	} else {
		?>
        <link rel="stylesheet" href="<?=get_template_directory_uri();?>/style-desktop.css">
		<?php
	}
	?>

</head>
<body <?php body_class($extraBodyClass)?>>

    <header>
        <nav>
            <ul>
                <li><a href="#">Link 1</a></li>
                <li><a href="#">Link 2</a></li>
                <li><a href="#">Link 3</a></li>
            </ul>
        </nav>
    </header>